CREATE TABLE `web_mgs` (
  `id` int(20) UNSIGNED NOT NULL,
  `web_medias_id` int(20) UNSIGNED NOT NULL,
  `web_msg_group_id` int(11) UNSIGNED NOT NULL,
  `content_category` int(20) UNSIGNED NOT NULL,
  `resource` varchar(50) NOT NULL,
  `resource_group` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `lang` varchar(6) NOT NULL DEFAULT 'de',
  `headline` text NOT NULL,
  `content` mediumtext NOT NULL,
  `publish_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_author` varchar(100) NOT NULL,
  `author_email` varchar(80) NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `comment_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `web_msg_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(150) NOT NULL,
  `scope` varchar(200) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- phpMyAdmin SQL Dump
-- version 4.6.2
-- https://www.phpmyadmin.net/
--
-- Host: 172.16.1.105
-- Erstellungszeit: 08. Jun 2018 um 14:49
-- Server-Version: 5.6.40-84.0-log
-- PHP-Version: 5.6.33-0+deb8u1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Datenbank: `db100153sql2`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `accounts`
--

CREATE TABLE `accounts` (
  `id` int(20) UNSIGNED NOT NULL,
  `fieldtypes_id` int(5) UNSIGNED NOT NULL,
  `account_id` char(36) NOT NULL,
  `parent_id` int(20) NOT NULL DEFAULT '0',
  `account_name` varchar(250) NOT NULL,
  `organisation` varchar(255) NOT NULL,
  `organisation_ext` varchar(250) NOT NULL,
  `organisation_scope` varchar(250) NOT NULL,
  `img_logo` varchar(250) NOT NULL,
  `img_source` varchar(250) NOT NULL,
  `img_large` varchar(250) NOT NULL,
  `account_fax` varchar(25) NOT NULL,
  `account_phone` varchar(25) NOT NULL,
  `phone_alternate` varchar(25) NOT NULL,
  `phone_mobile` varchar(50) NOT NULL,
  `account_email` varchar(100) NOT NULL,
  `account_street` varchar(250) NOT NULL,
  `account_street_number` varchar(15) NOT NULL,
  `account_addresss` varchar(250) NOT NULL,
  `account_zipcode` varchar(25) NOT NULL,
  `account_city` varchar(250) NOT NULL,
  `account_country` varchar(100) NOT NULL,
  `account_state` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `description_ext` text NOT NULL,
  `description_hidden` varchar(500) NOT NULL,
  `latitude` varchar(50) NOT NULL DEFAULT '51.1626598',
  `longitude` varchar(50) NOT NULL DEFAULT '10.450654600000007',
  `basedir` varchar(500) NOT NULL,
  `internet` varchar(250) NOT NULL,
  `params` text NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `webentry` varchar(10) NOT NULL DEFAULT 'default',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `event_location` int(1) NOT NULL DEFAULT '0',
  `event_organizer` int(1) NOT NULL DEFAULT '0',
  `occupancy_resource` int(1) NOT NULL DEFAULT '0',
  `municipal_account` int(1) NOT NULL DEFAULT '0',
  `municipal_local` int(1) NOT NULL DEFAULT '0',
  `municipal_directory` int(1) NOT NULL DEFAULT '0',
  `fw_account` tinyint(1) NOT NULL DEFAULT '0',
  `fw_resource` tinyint(1) NOT NULL DEFAULT '0',
  `disable` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `accounts`
--

INSERT INTO `accounts` (`id`, `fieldtypes_id`, `account_id`, `parent_id`, `account_name`, `organisation`, `organisation_ext`, `organisation_scope`, `img_logo`, `img_source`, `img_large`, `account_fax`, `account_phone`, `phone_alternate`, `phone_mobile`, `account_email`, `account_street`, `account_street_number`, `account_addresss`, `account_zipcode`, `account_city`, `account_country`, `account_state`, `description`, `description_ext`, `description_hidden`, `latitude`, `longitude`, `basedir`, `internet`, `params`, `publish`, `webentry`, `deleted`, `event_location`, `event_organizer`, `occupancy_resource`, `municipal_account`, `municipal_local`, `municipal_directory`, `fw_account`, `fw_resource`, `disable`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 1, '481f5071f1ae64a052b980adc9dba539', 0, 'maincontentusers', 'System Benutzer', '', 'content-managment-system-benutzer', '', '_nomedia', '', '', '06182 932 699', '', '', '', '', '', '', '', '', '', '', '', '', '', '51.1626598', '10.450654600000007', 'denied', '', '', 'no', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `account_contacts`
--

CREATE TABLE `account_contacts` (
  `id` int(11) UNSIGNED NOT NULL,
  `account_id` int(20) UNSIGNED NOT NULL,
  `contacts_id` int(11) UNSIGNED NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `account_groups`
--

CREATE TABLE `account_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `scope` varchar(250) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `image_source` varchar(250) NOT NULL,
  `headlines` varchar(150) NOT NULL,
  `sub_headline` text NOT NULL,
  `description` text NOT NULL,
  `params` text NOT NULL,
  `enable_organisation_ext` tinyint(1) NOT NULL DEFAULT '1',
  `enable_img_source` tinyint(1) NOT NULL DEFAULT '1',
  `enable_account_fax` tinyint(1) NOT NULL DEFAULT '1',
  `enable_account_phone` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_alternate` tinyint(1) NOT NULL DEFAULT '0',
  `enable_account_email` tinyint(1) NOT NULL DEFAULT '1',
  `enable_account_street` tinyint(1) NOT NULL DEFAULT '1',
  `enable_account_addresss` tinyint(1) NOT NULL DEFAULT '0',
  `enable_account_city` tinyint(1) NOT NULL DEFAULT '1',
  `enable_description` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `account_group_index`
--

CREATE TABLE `account_group_index` (
  `id` int(11) UNSIGNED NOT NULL,
  `index_group_id` int(10) UNSIGNED NOT NULL,
  `account_id` int(20) UNSIGNED NOT NULL,
  `publish` varchar(4) NOT NULL DEFAULT 'no',
  `item_rang` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `salutation` varchar(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `name_extension` varchar(200) NOT NULL,
  `object_name` varchar(250) NOT NULL,
  `business_title` varchar(250) NOT NULL,
  `just_sign` varchar(10) NOT NULL,
  `department` varchar(255) NOT NULL,
  `phone_home` varchar(25) NOT NULL,
  `phone_mobile` varchar(25) NOT NULL,
  `phone_work` varchar(25) NOT NULL,
  `phone_other` varchar(25) NOT NULL,
  `phone_fax` varchar(25) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `internet` text NOT NULL,
  `alternate_email` varchar(100) NOT NULL,
  `contact_chat` varchar(100) NOT NULL,
  `skype` varchar(150) NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `instagram` varchar(150) NOT NULL,
  `pinterest` varchar(150) NOT NULL,
  `contact_img_source` varchar(250) NOT NULL,
  `contact_img_large` varchar(250) NOT NULL,
  `contact_address` varchar(250) NOT NULL,
  `address_further` text NOT NULL,
  `contact_zipcode` varchar(25) NOT NULL,
  `contact_city` varchar(250) NOT NULL,
  `contact_country` varchar(250) NOT NULL,
  `contact_state` varchar(250) NOT NULL,
  `birthdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `floor` varchar(160) NOT NULL,
  `room` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `contact_type` varchar(10) NOT NULL DEFAULT 'contact',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `contacts`
--

INSERT INTO `contacts` (`id`, `title`, `salutation`, `first_name`, `last_name`, `name_extension`, `object_name`, `business_title`, `just_sign`, `department`, `phone_home`, `phone_mobile`, `phone_work`, `phone_other`, `phone_fax`, `contact_email`, `internet`, `alternate_email`, `contact_chat`, `skype`, `facebook`, `twitter`, `instagram`, `pinterest`, `contact_img_source`, `contact_img_large`, `contact_address`, `address_further`, `contact_zipcode`, `contact_city`, `contact_country`, `contact_state`, `birthdate`, `floor`, `room`, `description`, `publish`, `deleted`, `contact_type`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, '', '', 'System', 'User', '', '', '', '', '', '', '', '', '', '', 'support@jochum-mediaservices.de', '', '', '', '', '', '', '', '', '_nomedia', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', 'yes', 0, 'contact', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contact_groups`
--

CREATE TABLE `contact_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `accounts_id` int(20) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `scope` varchar(250) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `image_source` varchar(250) NOT NULL,
  `headlines` varchar(150) NOT NULL,
  `sub_headline` text NOT NULL,
  `enable_organame` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `enable_orglogo` tinyint(1) NOT NULL DEFAULT '1',
  `enable_orgaddress` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `enable_orgaphone` tinyint(1) NOT NULL DEFAULT '1',
  `enable_orgamail` tinyint(1) NOT NULL DEFAULT '1',
  `enable_orgainternet` tinyint(1) NOT NULL DEFAULT '1',
  `enable_title` tinyint(1) NOT NULL DEFAULT '1',
  `enable_salutation` tinyint(1) NOT NULL DEFAULT '1',
  `enable_first_name` tinyint(1) NOT NULL DEFAULT '1',
  `enable_last_name` tinyint(1) NOT NULL DEFAULT '1',
  `enable_business_title` tinyint(1) NOT NULL DEFAULT '1',
  `enable_just_sign` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_home` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_mobile` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_work` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_other` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_fax` tinyint(1) NOT NULL DEFAULT '1',
  `enable_contact_email` tinyint(1) NOT NULL DEFAULT '1',
  `enable_internet` tinyint(1) NOT NULL DEFAULT '1',
  `enable_alternate_email` tinyint(1) NOT NULL DEFAULT '1',
  `enable_chat` tinyint(1) NOT NULL DEFAULT '1',
  `enable_facebook` tinyint(1) NOT NULL DEFAULT '1',
  `enable_twitter` tinyint(1) NOT NULL DEFAULT '1',
  `enable_instagram` tinyint(1) NOT NULL DEFAULT '1',
  `enable_pinterest` tinyint(1) NOT NULL DEFAULT '1',
  `enable_image` tinyint(1) NOT NULL DEFAULT '1',
  `enable_address` tinyint(1) NOT NULL DEFAULT '1',
  `enable_birthdate` tinyint(1) NOT NULL DEFAULT '1',
  `enable_description` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contact_group_index`
--

CREATE TABLE `contact_group_index` (
  `id` int(20) UNSIGNED NOT NULL,
  `index_group_id` int(5) UNSIGNED NOT NULL,
  `contacts_id` int(11) UNSIGNED NOT NULL,
  `item_rang` int(5) UNSIGNED NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `business_title` text NOT NULL,
  `phone_mobile` varchar(25) NOT NULL,
  `phone_work` varchar(25) NOT NULL,
  `phone_fax` varchar(25) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `internet` text NOT NULL,
  `contact_chat` varchar(100) NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `contact_img_source` varchar(250) NOT NULL,
  `contact_address` varchar(250) NOT NULL,
  `contact_zipcode` varchar(25) NOT NULL,
  `contact_city` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `enable_title` tinyint(1) NOT NULL DEFAULT '1',
  `enable_salutation` tinyint(1) NOT NULL DEFAULT '1',
  `enable_first_name` tinyint(1) NOT NULL DEFAULT '1',
  `enable_last_name` tinyint(1) NOT NULL DEFAULT '1',
  `enable_business_title` tinyint(1) NOT NULL DEFAULT '1',
  `enable_just_sign` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_home` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_mobile` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_work` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_other` tinyint(1) NOT NULL DEFAULT '1',
  `enable_phone_fax` tinyint(1) NOT NULL DEFAULT '1',
  `enable_contact_email` tinyint(1) NOT NULL DEFAULT '1',
  `enable_internet` tinyint(1) NOT NULL DEFAULT '1',
  `enable_alternate_email` tinyint(1) NOT NULL DEFAULT '1',
  `enable_chat` tinyint(1) NOT NULL DEFAULT '1',
  `enable_facebook` tinyint(1) NOT NULL DEFAULT '1',
  `enable_twitter` tinyint(1) NOT NULL DEFAULT '1',
  `enable_instagram` tinyint(1) NOT NULL DEFAULT '1',
  `enable_pinterest` tinyint(1) NOT NULL DEFAULT '1',
  `enable_image` tinyint(1) NOT NULL DEFAULT '1',
  `enable_address` tinyint(1) NOT NULL DEFAULT '1',
  `enable_birthdate` tinyint(1) NOT NULL DEFAULT '1',
  `enable_description` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `update_by` int(11) NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `field_types`
--

CREATE TABLE `field_types` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `typescope` varchar(60) NOT NULL,
  `params` text NOT NULL,
  `select_login` int(1) NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `field_types`
--

INSERT INTO `field_types` (`id`, `name`, `typescope`, `params`, `select_login`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 'Accounts', 'accounts', '', 0, 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34'),
  (2, 'Organsiationen', 'organisation', '', 0, 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mcevent_dates`
--

CREATE TABLE `mcevent_dates` (
  `id` int(11) UNSIGNED NOT NULL,
  `calendar_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `configure_id` int(5) NOT NULL DEFAULT '0',
  `serial_id` varchar(36) NOT NULL DEFAULT '0',
  `organizer_id` int(20) UNSIGNED NOT NULL,
  `resource_id` int(20) UNSIGNED NOT NULL,
  `account_id` int(20) UNSIGNED NOT NULL,
  `summary` varchar(255) NOT NULL,
  `organizer` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `location_addresse` varchar(250) NOT NULL,
  `location_zipcode` varchar(25) NOT NULL,
  `location_city` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `findlocation` varchar(250) NOT NULL,
  `latitude` varchar(250) NOT NULL,
  `longitude` varchar(250) NOT NULL,
  `web_medias_id` int(20) UNSIGNED NOT NULL,
  `web_files_id` int(20) UNSIGNED NOT NULL,
  `download_label` varchar(200) NOT NULL,
  `info_url` varchar(250) NOT NULL,
  `params` text NOT NULL,
  `date_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `comment_status` varchar(4) NOT NULL DEFAULT 'no',
  `rating_status` varchar(4) NOT NULL DEFAULT 'no',
  `status` varchar(12) NOT NULL DEFAULT 'free',
  `access` varchar(5) NOT NULL DEFAULT 'close',
  `occupancy` char(1) NOT NULL DEFAULT 'n',
  `public_view` varchar(3) NOT NULL DEFAULT 'no',
  `applicant_int` int(4) NOT NULL DEFAULT '0',
  `applicant_ext` int(4) NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mcevent_dates_config`
--

CREATE TABLE `mcevent_dates_config` (
  `id` int(5) UNSIGNED NOT NULL,
  `organizer_id` int(20) UNSIGNED NOT NULL,
  `name` varchar(150) NOT NULL,
  `send_email_participant` int(1) NOT NULL DEFAULT '0',
  `email_participant` text NOT NULL,
  `send_email_organizer` int(1) NOT NULL DEFAULT '0',
  `email_organizer` text NOT NULL,
  `emailsubject` varchar(150) NOT NULL,
  `emailsignature` varchar(150) NOT NULL,
  `settings_formular` text NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mcevent_dates_register`
--

CREATE TABLE `mcevent_dates_register` (
  `id` int(11) UNSIGNED NOT NULL,
  `register_ident` varchar(40) NOT NULL,
  `organizer_id` int(20) UNSIGNED NOT NULL,
  `mcevent_id` int(20) UNSIGNED NOT NULL,
  `company` varchar(250) NOT NULL,
  `title` varchar(5) NOT NULL,
  `surname` varchar(80) NOT NULL,
  `name` varchar(250) NOT NULL,
  `street` varchar(250) NOT NULL,
  `number` varchar(10) NOT NULL,
  `city` varchar(80) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `post_street` varchar(150) NOT NULL,
  `post_street_num` varchar(10) NOT NULL,
  `post_city` varchar(150) NOT NULL,
  `post_zipcode` varchar(10) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `merkblatt` varchar(250) NOT NULL,
  `num_groupmember` int(4) NOT NULL DEFAULT '0',
  `stay_breakfast` varchar(10) NOT NULL,
  `stay_lunch` varchar(10) NOT NULL,
  `stay_dinner` varchar(10) NOT NULL,
  `stay_overnight` varchar(10) NOT NULL,
  `params` varchar(250) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `update_by` int(11) UNSIGNED NOT NULL,
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mcevent_group`
--

CREATE TABLE `mcevent_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(250) NOT NULL,
  `scope` varchar(250) NOT NULL,
  `params` text NOT NULL,
  `calc_key` varchar(40) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mcevent_index`
--

CREATE TABLE `mcevent_index` (
  `id` int(11) UNSIGNED NOT NULL,
  `groups_id` int(11) UNSIGNED NOT NULL,
  `calendar_id` int(11) UNSIGNED NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'yes',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mcevent_types`
--

CREATE TABLE `mcevent_types` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `headline` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `calc_key` varchar(40) NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mediainuse`
--

CREATE TABLE `mediainuse` (
  `mediasid` int(20) UNSIGNED NOT NULL,
  `inuseid` int(20) UNSIGNED NOT NULL,
  `groupname` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users5`
--

CREATE TABLE `users5` (
  `id` int(11) UNSIGNED NOT NULL,
  `contacts_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `user_roles_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `apps` text NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `initials` varchar(10) NOT NULL,
  `language` varchar(6) NOT NULL DEFAULT 'de',
  `avatar` varchar(50) NOT NULL,
  `login_homedir` varchar(250) NOT NULL DEFAULT '/mcwork/dashboard',
  `user_home` varchar(250) NOT NULL,
  `logout_path` varchar(200) NOT NULL DEFAULT '/',
  `username` varchar(150) NOT NULL,
  `login_username` varchar(150) NOT NULL,
  `login_password` varchar(150) NOT NULL,
  `login_password_alt` varchar(150) NOT NULL DEFAULT '0',
  `get_password` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `verify_string` varchar(50) NOT NULL DEFAULT '0',
  `try_login` int(3) NOT NULL DEFAULT '0',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_logout` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `count_login` int(5) NOT NULL DEFAULT '0',
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `users5`
--

INSERT INTO `users5` (`id`, `contacts_id`, `user_roles_id`, `params`, `apps`, `name`, `email`, `initials`, `language`, `avatar`, `login_homedir`, `user_home`, `logout_path`, `username`, `login_username`, `login_password`, `login_password_alt`, `get_password`, `verify_string`, `try_login`, `last_login`, `last_logout`, `count_login`, `publish`, `deleted`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 1, 2, '', '', '', '', '', 'de', '', '/mcwork/dashboard', 'root', '/', 'root', '63a9f0ea7bb98050796b649e85481845', '$6$4EgJoh2/LenGD8K9$djPnGXhtsS3ACshlhTOn/7GUyHbtDsNO2ju1j2PaJNovzpwgKaXEXo2yCSDtDRILk4Cj2u0kw70rGW7YKGHdA.', '', 0, '4EgJoh2/LenGD8K9', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'yes', 0, 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_acl_groups`
--

CREATE TABLE `user_acl_groups` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `groupscope` varchar(60) NOT NULL,
  `params` text NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user_acl_groups`
--

INSERT INTO `user_acl_groups` (`id`, `name`, `groupscope`, `params`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 'Authentifizierte Benutzer', 'authuser', '', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_acl_index`
--

CREATE TABLE `user_acl_index` (
  `id` int(20) UNSIGNED NOT NULL,
  `users_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `acl_group_id` int(5) UNSIGNED NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `update_by` int(11) NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user_acl_index`
--

INSERT INTO `user_acl_index` (`id`, `users_id`, `acl_group_id`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 1, 1, 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_roles`
--

CREATE TABLE `user_roles` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `scope` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `scope`) VALUES
  (1, 'Root', 'root'),
  (2, 'Administrator', 'admin'),
  (3, 'Manager', 'manager'),
  (4, 'Publisher', 'publisher'),
  (5, 'Author', 'author'),
  (6, 'Intranet', 'intranet'),
  (7, 'Member', 'member'),
  (8, 'Guest', 'guest');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_content`
--

CREATE TABLE `web_content` (
  `id` int(20) UNSIGNED NOT NULL,
  `web_medias_id` int(20) UNSIGNED NOT NULL,
  `web_contentgroup_id` int(20) UNSIGNED NOT NULL,
  `web_pages_id` int(20) UNSIGNED NOT NULL,
  `content_category` int(20) UNSIGNED NOT NULL,
  `htmlwidgets` varchar(50) NOT NULL,
  `element` varchar(20) NOT NULL,
  `element_attribute` text NOT NULL,
  `element_content` text NOT NULL,
  `resource` varchar(50) NOT NULL,
  `resource_group` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `source` text NOT NULL,
  `modul` varchar(60) NOT NULL,
  `modul_params` text NOT NULL,
  `modul_display` varchar(250) NOT NULL,
  `modul_config` varchar(250) NOT NULL,
  `modul_link` varchar(250) NOT NULL,
  `modul_format` varchar(80) NOT NULL,
  `media_link_page` int(20) UNSIGNED NOT NULL,
  `media_link_group` int(20) UNSIGNED NOT NULL,
  `media_link_url` varchar(250) NOT NULL,
  `media_style` varchar(250) NOT NULL,
  `media_placeholder` tinyint(1) NOT NULL DEFAULT '0',
  `lang` varchar(6) NOT NULL DEFAULT 'de',
  `title` text NOT NULL,
  `headline` text NOT NULL,
  `question` text NOT NULL,
  `content_teaser` text NOT NULL,
  `content` mediumtext NOT NULL,
  `number_character_teaser` int(5) UNSIGNED NOT NULL,
  `label_read_more` varchar(200) NOT NULL,
  `publish_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_author` varchar(100) NOT NULL,
  `author_email` varchar(80) NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `publish_app` varchar(4) NOT NULL DEFAULT 'yes',
  `comment_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `version` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `replace_default` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `overwrite` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `updateflag` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_content`
--

INSERT INTO `web_content` (`id`, `web_medias_id`, `web_contentgroup_id`, `web_pages_id`, `content_category`, `htmlwidgets`, `element`, `element_attribute`, `element_content`, `resource`, `resource_group`, `source`, `modul`, `modul_params`, `modul_display`, `modul_config`, `modul_link`, `modul_format`, `media_link_page`, `media_link_group`, `media_link_url`, `media_style`, `media_placeholder`, `lang`, `title`, `headline`, `question`, `content_teaser`, `content`, `number_character_teaser`, `label_read_more`, `publish_date`, `publish_author`, `author_email`, `publish`, `publish_up`, `publish_down`, `publish_app`, `comment_status`, `version`, `replace_default`, `overwrite`, `deleted`, `updateflag`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 1, 0, 0, 0, 'content', '', '', '', 'index', 0, '', '', '', '', '', '', '', 0, 0, '', '', 0, 'de', 'Standard Meldung wenn keine Inhalte', '', '', '', '<p><strong>An den Inhalten wird zur Zeit gearbeitet.</strong></p>', 0, '', '0000-00-00 00:00:00', '', '', 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'yes', 0, 0, 0, 1, 0, 0, 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34'),
  (2, 1, 0, 0, 0, 'content', '', '', '', 'index', 0, '', '', '', '', '', '', '', 0, 0, '', '', 0, 'de', 'Standard Seiten Header', '', '', '', '<h1>Webseiten Header</h1>', 0, '', '0000-00-00 00:00:00', '', '', 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'yes', 0, 0, 0, 0, 0, 0, 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34'),
  (3, 1, 0, 0, 0, 'content', '', '', '', 'index', 0, '', '', '', '', '', '', '', 0, 0, '', '', 0, 'de', 'Standard Seiten Footer', '', '', '', '<h3>Webseiten Footer</h3>', 0, '', '0000-00-00 00:00:00', '', '', 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'yes', 0, 0, 0, 0, 0, 0, 2, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_content_attribute`
--

CREATE TABLE `web_content_attribute` (
  `id` int(20) UNSIGNED NOT NULL,
  `web_content_id` int(20) UNSIGNED NOT NULL,
  `modul` varchar(60) NOT NULL,
  `modul_params` tinytext NOT NULL,
  `modul_display` varchar(250) NOT NULL DEFAULT '0',
  `modul_config` varchar(250) NOT NULL,
  `number_letter` varchar(5) NOT NULL DEFAULT '0',
  `num_letter_title` varchar(100) NOT NULL,
  `publish_date` date NOT NULL DEFAULT '0000-00-00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_content_groups`
--

CREATE TABLE `web_content_groups` (
  `id` int(20) UNSIGNED NOT NULL,
  `headline_images` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `logo_images` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(150) NOT NULL DEFAULT '_default',
  `scope` varchar(15) NOT NULL DEFAULT 'content',
  `content_group_name` varchar(15) NOT NULL DEFAULT 'content',
  `content_group_page` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `web_contentgroup_id` int(20) UNSIGNED NOT NULL,
  `web_content_id` int(20) UNSIGNED NOT NULL,
  `group_style` varchar(50) NOT NULL DEFAULT 'none',
  `group_element` varchar(20) NOT NULL,
  `group_element_attribute` text NOT NULL,
  `item_rang` int(5) UNSIGNED NOT NULL,
  `publish_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `group_params` text NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_content_groups`
--

INSERT INTO `web_content_groups` (`id`, `headline_images`, `logo_images`, `name`, `scope`, `content_group_name`, `content_group_page`, `web_contentgroup_id`, `web_content_id`, `group_style`, `group_element`, `group_element_attribute`, `item_rang`, `publish_date`, `group_params`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 0, 0, '_default', 'content', 'content', 0, 1, 1, 'none', '', '', 1, '0000-00-00 00:00:00', '', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34'),
  (2, 0, 0, '_default', 'content', 'content', 0, 2, 2, 'none', '', '', 1, '0000-00-00 00:00:00', '', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34'),
  (3, 0, 0, '_default', 'content', 'content', 0, 3, 3, 'none', '', '', 1, '0000-00-00 00:00:00', '', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_content_indexgroup`
--

CREATE TABLE `web_content_indexgroup` (
  `id` int(11) UNSIGNED NOT NULL,
  `groups_id` int(11) UNSIGNED NOT NULL,
  `indexgroup_id` int(11) UNSIGNED NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_content_namegroup`
--

CREATE TABLE `web_content_namegroup` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `feed_title` varchar(250) NOT NULL,
  `feed_author` varchar(100) NOT NULL,
  `feed_authoremail` varchar(250) NOT NULL,
  `feed_authorinternet` varchar(250) NOT NULL,
  `feed_count` int(5) NOT NULL DEFAULT '0',
  `feed_url` varchar(250) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_forms`
--

CREATE TABLE `web_forms` (
  `id` int(11) UNSIGNED NOT NULL,
  `headline` varchar(250) NOT NULL,
  `htmlwidgets` varchar(50) NOT NULL,
  `resource` varchar(50) NOT NULL,
  `subheadline` varchar(250) NOT NULL,
  `script` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `responsetext` text NOT NULL,
  `emailsubject` varchar(250) NOT NULL,
  `email` text NOT NULL,
  `emailname` varchar(250) NOT NULL,
  `emailcc` varchar(250) NOT NULL,
  `sendfrom` varchar(150) NOT NULL,
  `replayemail` varchar(100) NOT NULL,
  `replayname` varchar(60) NOT NULL,
  `emailtext` text NOT NULL,
  `email_template` varchar(300) NOT NULL,
  `created_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `register_date` datetime NOT NULL,
  `up_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_forms_field`
--

CREATE TABLE `web_forms_field` (
  `id` int(20) UNSIGNED NOT NULL,
  `web_forms_id` int(11) UNSIGNED NOT NULL,
  `web_medias_id` int(20) UNSIGNED NOT NULL,
  `resource` varchar(50) NOT NULL,
  `label` varchar(250) NOT NULL,
  `field_typ` varchar(250) NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `field_value` varchar(250) NOT NULL,
  `field_required` varchar(10) NOT NULL,
  `field_domid` varchar(50) NOT NULL,
  `field_class` varchar(100) NOT NULL,
  `fieldset` int(1) NOT NULL DEFAULT '0',
  `fieldset_legend` varchar(250) NOT NULL,
  `fieldset_attributes` text NOT NULL,
  `attributes` text NOT NULL,
  `description` text NOT NULL,
  `select_options` text NOT NULL,
  `item_rang` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `created_by` int(11) UNSIGNED NOT NULL,
  `update_by` int(11) UNSIGNED NOT NULL,
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_forms_fieldoptions`
--

CREATE TABLE `web_forms_fieldoptions` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_field_id` int(20) UNSIGNED NOT NULL,
  `option_group` varchar(250) NOT NULL DEFAULT ' ',
  `option_value` varchar(250) NOT NULL DEFAULT ' ',
  `option_label` varchar(250) NOT NULL DEFAULT ' ',
  `option_labeltitle` varchar(250) NOT NULL DEFAULT ' ',
  `img_source` varchar(250) NOT NULL DEFAULT ' ',
  `created_by` int(11) UNSIGNED NOT NULL,
  `update_by` int(11) UNSIGNED NOT NULL,
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_maps`
--

CREATE TABLE `web_maps` (
  `id` int(11) UNSIGNED NOT NULL,
  `headline` varchar(250) NOT NULL,
  `htmlwidgets` varchar(50) NOT NULL,
  `subheadline` varchar(250) NOT NULL,
  `mapkey` varchar(250) NOT NULL,
  `location` text NOT NULL,
  `centerlatitude` varchar(40) NOT NULL DEFAULT '51.165691',
  `centerlongitude` varchar(40) NOT NULL DEFAULT '10.451526',
  `startzoom` tinyint(3) UNSIGNED NOT NULL DEFAULT '5',
  `script` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_maps_data`
--

CREATE TABLE `web_maps_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `web_maps_id` int(11) UNSIGNED NOT NULL,
  `web_medias_id` int(20) UNSIGNED NOT NULL,
  `map_marker` varchar(250) NOT NULL DEFAULT '_nomedia',
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `startzoom` tinyint(3) UNSIGNED NOT NULL DEFAULT '12',
  `name` varchar(250) NOT NULL,
  `street` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `url` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `business_hours` text NOT NULL,
  `business_hours_use` varchar(4) NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `created_by` int(11) UNSIGNED NOT NULL,
  `update_by` int(11) UNSIGNED NOT NULL,
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_mediagroup_combine`
--

CREATE TABLE `web_mediagroup_combine` (
  `id` int(11) UNSIGNED NOT NULL,
  `web_groups_id` int(20) UNSIGNED NOT NULL,
  `web_mediagroup_id` int(20) UNSIGNED NOT NULL,
  `item_rang` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `resource` varchar(50) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_medias`
--

CREATE TABLE `web_medias` (
  `id` int(20) UNSIGNED NOT NULL,
  `parent_media` int(20) NOT NULL DEFAULT '0',
  `media_name` varchar(100) NOT NULL,
  `media_source` varchar(500) NOT NULL,
  `media_link` varchar(250) NOT NULL,
  `media_type` varchar(50) NOT NULL,
  `media_attribute` text NOT NULL,
  `media_alternate` mediumtext NOT NULL,
  `media_metas` longtext NOT NULL,
  `media_sizes` varchar(250) NOT NULL,
  `media_dimensions` varchar(250) NOT NULL,
  `media_description` text NOT NULL,
  `meta_coding` varchar(20) NOT NULL,
  `resource` varchar(50) NOT NULL DEFAULT 'index',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_medias`
--

INSERT INTO `web_medias` (`id`, `parent_media`, `media_name`, `media_source`, `media_link`, `media_type`, `media_attribute`, `media_alternate`, `media_metas`, `media_sizes`, `media_dimensions`, `media_description`, `meta_coding`, `resource`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 0, 'No Media', '_nomedia', '_nomedia', '_nomedia', '', '', '', '', '', '', '', 'index', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_media_categories`
--

CREATE TABLE `web_media_categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `web_medias_id` int(20) UNSIGNED NOT NULL,
  `web_mediagroup_id` int(20) UNSIGNED NOT NULL,
  `parent_media_file` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `item_rang` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `caption` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `media_link_url` varchar(250) NOT NULL,
  `target_link` varchar(20) NOT NULL,
  `resource` varchar(50) NOT NULL,
  `alternate_download` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `alternate_linktitle` varchar(250) NOT NULL,
  `alternate_labelname` varchar(250) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_media_group`
--

CREATE TABLE `web_media_group` (
  `id` int(20) UNSIGNED NOT NULL,
  `group_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `resource` varchar(50) NOT NULL,
  `htmlwidgets` varchar(50) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_media_groups`
--

CREATE TABLE `web_media_groups` (
  `id` int(20) UNSIGNED NOT NULL,
  `groups_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `resource` varchar(50) NOT NULL,
  `htmlwidgets` varchar(50) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_navigations`
--

CREATE TABLE `web_navigations` (
  `id` int(11) UNSIGNED NOT NULL,
  `tree_ident` varchar(250) NOT NULL,
  `tpl_assign` varchar(25) NOT NULL,
  `htmlwidgets` varchar(50) NOT NULL,
  `menue` varchar(8) NOT NULL,
  `title` varchar(150) NOT NULL,
  `headline` text NOT NULL,
  `tags` varchar(2) NOT NULL,
  `params` text NOT NULL,
  `sitemap` varchar(5) NOT NULL DEFAULT 'no',
  `language` varchar(10) NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_navigations`
--

INSERT INTO `web_navigations` (`id`, `tree_ident`, `tpl_assign`, `htmlwidgets`, `menue`, `title`, `headline`, `tags`, `params`, `sitemap`, `language`, `publish`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 'hauptnavigation', 'STANDARD', 'none', 'std', 'Navigationsbaum Webseite', 'Navigationsbaum Webseite', 'h2', '', 'yes', '', 'no', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_navigation_tree`
--

CREATE TABLE `web_navigation_tree` (
  `id` int(11) UNSIGNED NOT NULL,
  `web_navigation_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `web_pages_id` int(20) UNSIGNED NOT NULL,
  `parent_from` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `item_rang` int(5) UNSIGNED NOT NULL DEFAULT '0',
  `alternate_labelname` text NOT NULL,
  `rel_link` varchar(50) NOT NULL DEFAULT 'no',
  `target_link` varchar(20) NOT NULL,
  `class_link` varchar(100) NOT NULL,
  `data_link` varchar(250) NOT NULL,
  `dom_id` varchar(50) NOT NULL,
  `style_class` varchar(50) NOT NULL,
  `params` text NOT NULL,
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `resource` varchar(50) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_navigation_tree`
--

INSERT INTO `web_navigation_tree` (`id`, `web_navigation_id`, `web_pages_id`, `parent_from`, `item_rang`, `alternate_labelname`, `rel_link`, `target_link`, `class_link`, `data_link`, `dom_id`, `style_class`, `params`, `publish`, `resource`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 1, 2, 0, 1, '', '', '', '', '', '', '', '', 'yes', 'index', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_pages_attributes`
--

CREATE TABLE `web_pages_attributes` (
  `id` int(20) UNSIGNED NOT NULL,
  `web_pages_id` int(20) UNSIGNED NOT NULL,
  `template` varchar(250) NOT NULL,
  `assets` varchar(250) NOT NULL,
  `charset` varchar(20) NOT NULL,
  `favicon` varchar(150) NOT NULL,
  `body_id` varchar(250) NOT NULL,
  `body_class` varchar(250) NOT NULL,
  `body_data_attribute` text NOT NULL,
  `head_script` text NOT NULL,
  `head_style` text NOT NULL,
  `inline_script` text NOT NULL,
  `meta_docstart` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_viewport` varchar(100) NOT NULL,
  `nocache` char(1) NOT NULL DEFAULT 'n',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_pages_attributes`
--

INSERT INTO `web_pages_attributes` (`id`, `web_pages_id`, `template`, `assets`, `charset`, `favicon`, `body_id`, `body_class`, `body_data_attribute`, `head_script`, `head_style`, `inline_script`, `meta_docstart`, `meta_keywords`, `meta_viewport`, `nocache`, `publish_up`, `publish_down`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 1, '', '', 'utf-8', '', '', '', '', '', '', '', '', '', 'width=device-width, initial-scale=1.0', 'n', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '2017-10-27 14:48:34', '2017-10-27 14:48:34');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_pages_content`
--

CREATE TABLE `web_pages_content` (
  `id` int(20) UNSIGNED NOT NULL,
  `web_pages_id` int(20) UNSIGNED NOT NULL,
  `web_contentgroup_id` int(20) UNSIGNED NOT NULL,
  `item_rang` int(5) NOT NULL DEFAULT '0',
  `content_rang` int(5) NOT NULL DEFAULT '0',
  `htmlwidgets` varchar(50) NOT NULL,
  `adjustments` text NOT NULL,
  `file` varchar(250) NOT NULL,
  `tpl_assign` varchar(20) NOT NULL DEFAULT 'content',
  `medias` tinyint(1) UNSIGNED DEFAULT '1',
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_pages_content`
--

INSERT INTO `web_pages_content` (`id`, `web_pages_id`, `web_contentgroup_id`, `item_rang`, `content_rang`, `htmlwidgets`, `adjustments`, `file`, `tpl_assign`, `medias`, `publish`, `publish_up`, `publish_down`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 1, 1, 5, 400, 'content', 'CONTENT', '', 'allcontent', 1, 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '2017-10-27 14:48:35', '2017-10-27 14:48:35'),
  (2, 1, 2, 1, 400, 'header', 'HEADER', '', 'allcontent', 1, 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '2017-10-27 14:48:35', '2017-10-27 14:48:35'),
  (3, 1, 3, 6, 400, 'footer', 'FOOTER', '', 'allcontent', 1, 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '2017-10-27 14:48:35', '2017-10-27 14:48:35');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_pages_groups`
--

CREATE TABLE `web_pages_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `group_id` int(20) UNSIGNED NOT NULL,
  `web_pages_id` int(11) UNSIGNED NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_pages_headlinks`
--

CREATE TABLE `web_pages_headlinks` (
  `id` int(20) UNSIGNED NOT NULL,
  `linkgroup` int(2) NOT NULL,
  `web_medias_id` int(20) UNSIGNED NOT NULL,
  `web_pages_id` int(20) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `meta_rel` varchar(250) NOT NULL,
  `meta_size` varchar(50) NOT NULL,
  `meta_type` varchar(20) NOT NULL,
  `meta_link` varchar(250) NOT NULL,
  `meta_name` varchar(250) NOT NULL,
  `meta_content` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  `item_rang` int(5) NOT NULL DEFAULT '0',
  `publish` varchar(10) NOT NULL DEFAULT 'yes',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_pages_maintenance`
--

CREATE TABLE `web_pages_maintenance` (
  `id` int(20) UNSIGNED NOT NULL,
  `web_pages_id` int(20) UNSIGNED NOT NULL,
  `maintenance` text NOT NULL,
  `maintenance_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `maintenance_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_pages_parameter`
--

CREATE TABLE `web_pages_parameter` (
  `id` int(20) UNSIGNED NOT NULL,
  `web_preferences_id` int(11) UNSIGNED NOT NULL,
  `host_id` varchar(36) NOT NULL,
  `parent_page` int(20) UNSIGNED NOT NULL,
  `template` varchar(250) NOT NULL,
  `assets` varchar(250) NOT NULL,
  `scope` varchar(255) NOT NULL,
  `resource` varchar(50) NOT NULL,
  `label` varchar(255) NOT NULL,
  `link_title` varchar(100) NOT NULL,
  `url` varchar(250) NOT NULL,
  `params` text NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `robots` varchar(60) NOT NULL,
  `changefreq` varchar(15) NOT NULL DEFAULT 'monthly',
  `priority` varchar(5) NOT NULL DEFAULT '0.8',
  `publish` varchar(10) NOT NULL DEFAULT 'no',
  `language` varchar(10) NOT NULL,
  `language_parent` int(20) UNSIGNED NOT NULL,
  `onlylink` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_pages_parameter`
--

INSERT INTO `web_pages_parameter` (`id`, `web_preferences_id`, `host_id`, `parent_page`, `template`, `assets`, `scope`, `resource`, `label`, `link_title`, `url`, `params`, `meta_title`, `meta_description`, `meta_keywords`, `robots`, `changefreq`, `priority`, `publish`, `language`, `language_parent`, `onlylink`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, 1, '', 0, '', '', '_default', 'index', '_default Defaults', '', '_default', '', '', '', '', '', 'monthly', '0.8', 'no', 'de', 0, 9, 1, 1, '2017-10-27 14:48:35', '2017-10-27 14:48:35'),
  (2, 1, '', 1, '', '', 'index', 'index', 'Start', '', 'index', '', '', '', '', 'index,follow', 'monthly', '0.8', 'yes', 'de', 0, 0, 1, 1, '2017-10-27 14:48:35', '2017-10-27 14:48:35');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_preferences`
--

CREATE TABLE `web_preferences` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` varchar(36) NOT NULL,
  `host` varchar(250) NOT NULL,
  `standard_domain` varchar(3) NOT NULL DEFAULT 'no',
  `title` varchar(140) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `author` varchar(250) NOT NULL,
  `locale` varchar(40) NOT NULL,
  `googleaccount` varchar(200) NOT NULL DEFAULT ' ',
  `siteverification` varchar(200) NOT NULL DEFAULT ' ',
  `head_script` text NOT NULL,
  `body_footer_script` text NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `web_preferences`
--

INSERT INTO `web_preferences` (`id`, `host_id`, `host`, `standard_domain`, `title`, `meta_description`, `meta_keywords`, `author`, `locale`, `googleaccount`, `siteverification`, `head_script`, `body_footer_script`, `created_by`, `update_by`, `register_date`, `up_date`) VALUES
  (1, '_default', '_default', 'yes', 'website title', '', '', 'website author', 'de_DE', ' ', '', '', '', 1, 1, '2017-10-27 14:48:35', '2017-10-27 14:48:35');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_redirect`
--

CREATE TABLE `web_redirect` (
  `id` int(5) UNSIGNED NOT NULL,
  `web_pages_id` int(20) UNSIGNED NOT NULL,
  `redirect` varchar(250) NOT NULL,
  `statuscode` int(3) NOT NULL DEFAULT '301',
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_tags`
--

CREATE TABLE `web_tags` (
  `id` int(20) UNSIGNED NOT NULL,
  `tag_group` varchar(200) NOT NULL,
  `tag_name` varchar(200) NOT NULL,
  `tag_scope` varchar(250) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `update_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `web_tags_assign`
--

CREATE TABLE `web_tags_assign` (
  `id` int(20) UNSIGNED NOT NULL,
  `tag_area` varchar(200) NOT NULL,
  `web_item_id` int(20) UNSIGNED NOT NULL,
  `web_itemgroup_id` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `web_tag_id` int(20) UNSIGNED NOT NULL,
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `up_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ORGANISATION` (`organisation`),
  ADD KEY `FIELDTYPESREF` (`fieldtypes_id`),
  ADD KEY `ACCOUNTIDENT` (`account_id`),
  ADD KEY `ACCOUNTSCOPE` (`account_name`);

--
-- Indizes für die Tabelle `account_contacts`
--
ALTER TABLE `account_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `account_groups`
--
ALTER TABLE `account_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `scope` (`scope`);

--
-- Indizes für die Tabelle `account_group_index`
--
ALTER TABLE `account_group_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `INDEXACCOUNTIDENT` (`account_id`),
  ADD KEY `ACCOUNTGROUP` (`index_group_id`);

--
-- Indizes für die Tabelle `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CONTACTNAME` (`last_name`);

--
-- Indizes für die Tabelle `contact_groups`
--
ALTER TABLE `contact_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `scope` (`scope`),
  ADD KEY `ACCOUNTGRPIDENT` (`accounts_id`);

--
-- Indizes für die Tabelle `contact_group_index`
--
ALTER TABLE `contact_group_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CONTACTGROUP` (`index_group_id`),
  ADD KEY `GRPCONTACTIDENT` (`contacts_id`);

--
-- Indizes für die Tabelle `field_types`
--
ALTER TABLE `field_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `TYPESCOPE` (`typescope`);

--
-- Indizes für die Tabelle `mcevent_dates`
--
ALTER TABLE `mcevent_dates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MCEVENTCALIDENT` (`calendar_id`),
  ADD KEY `ORGANIZERIDENT` (`organizer_id`),
  ADD KEY `LOCATIONIDENT` (`account_id`);

--
-- Indizes für die Tabelle `mcevent_dates_config`
--
ALTER TABLE `mcevent_dates_config`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `mcevent_dates_register`
--
ALTER TABLE `mcevent_dates_register`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `mcevent_group`
--
ALTER TABLE `mcevent_group`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `mcevent_index`
--
ALTER TABLE `mcevent_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CALENDARGROUP` (`groups_id`),
  ADD KEY `CALGROUPITEM` (`calendar_id`);

--
-- Indizes für die Tabelle `mcevent_types`
--
ALTER TABLE `mcevent_types`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `mediainuse`
--
ALTER TABLE `mediainuse`
  ADD PRIMARY KEY (`mediasid`,`inuseid`,`groupname`);

--
-- Indizes für die Tabelle `users5`
--
ALTER TABLE `users5`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `USERNAME` (`username`),
  ADD UNIQUE KEY `USERCONTACT` (`contacts_id`),
  ADD KEY `USERROLE` (`user_roles_id`);

--
-- Indizes für die Tabelle `user_acl_groups`
--
ALTER TABLE `user_acl_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GROUPSCOPE` (`groupscope`);

--
-- Indizes für die Tabelle `user_acl_index`
--
ALTER TABLE `user_acl_index`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ACLUSERINDEX` (`users_id`),
  ADD KEY `ACLUSERGROUPINDEX` (`acl_group_id`);

--
-- Indizes für die Tabelle `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `GROUPSCOPE` (`scope`);

--
-- Indizes für die Tabelle `web_content`
--
ALTER TABLE `web_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `WEBMEDIAS` (`web_medias_id`),
  ADD KEY `TITLEINTERNALUSE` (`title`(255)),
  ADD KEY `HEADLINEPUBLICUSE` (`headline`(255));

--
-- Indizes für die Tabelle `web_content_attribute`
--
ALTER TABLE `web_content_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `web_content_groups`
--
ALTER TABLE `web_content_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `CONTENTGROUP` (`web_contentgroup_id`),
  ADD KEY `GROUPREFCONTENT` (`web_content_id`);

--
-- Indizes für die Tabelle `web_content_indexgroup`
--
ALTER TABLE `web_content_indexgroup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NAMEGROUP` (`groups_id`),
  ADD KEY `NAMEGROUPITEM` (`indexgroup_id`);

--
-- Indizes für die Tabelle `web_content_namegroup`
--
ALTER TABLE `web_content_namegroup`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `web_forms`
--
ALTER TABLE `web_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FORMNAME` (`headline`);

--
-- Indizes für die Tabelle `web_forms_field`
--
ALTER TABLE `web_forms_field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FORMSPARENT` (`web_forms_id`),
  ADD KEY `FORMSMEDIA` (`web_medias_id`);

--
-- Indizes für die Tabelle `web_forms_fieldoptions`
--
ALTER TABLE `web_forms_fieldoptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FORMFIELDIDENT` (`form_field_id`);

--
-- Indizes für die Tabelle `web_maps`
--
ALTER TABLE `web_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MAPSNAME` (`headline`);

--
-- Indizes für die Tabelle `web_maps_data`
--
ALTER TABLE `web_maps_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MAPSPARENT` (`web_maps_id`),
  ADD KEY `MAPSMEDIA` (`web_medias_id`);

--
-- Indizes für die Tabelle `web_mediagroup_combine`
--
ALTER TABLE `web_mediagroup_combine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MEDIAGROUPIDENT` (`web_mediagroup_id`),
  ADD KEY `MEDIAGROUPSIDENT` (`web_groups_id`);

--
-- Indizes für die Tabelle `web_medias`
--
ALTER TABLE `web_medias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MEDIANAME` (`media_name`),
  ADD KEY `MEDIASOURCE` (`media_source`(255));

--
-- Indizes für die Tabelle `web_media_categories`
--
ALTER TABLE `web_media_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MEDIAGROUPID` (`web_mediagroup_id`),
  ADD KEY `GROUPMEDIAID` (`web_medias_id`);

--
-- Indizes für die Tabelle `web_media_group`
--
ALTER TABLE `web_media_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `MEDIAGROUP` (`group_name`);

--
-- Indizes für die Tabelle `web_media_groups`
--
ALTER TABLE `web_media_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `MEDIAGROUPS` (`groups_name`);

--
-- Indizes für die Tabelle `web_navigations`
--
ALTER TABLE `web_navigations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `NAVIGATION` (`tree_ident`);

--
-- Indizes für die Tabelle `web_navigation_tree`
--
ALTER TABLE `web_navigation_tree`
  ADD PRIMARY KEY (`id`),
  ADD KEY `PAGES` (`web_pages_id`),
  ADD KEY `PARENTFROM` (`parent_from`),
  ADD KEY `TREES` (`web_navigation_id`);

--
-- Indizes für die Tabelle `web_pages_attributes`
--
ALTER TABLE `web_pages_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `PAGEATTRIBPARENT` (`web_pages_id`);

--
-- Indizes für die Tabelle `web_pages_content`
--
ALTER TABLE `web_pages_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `PAGEIDENT` (`web_pages_id`),
  ADD KEY `CONTENTGROUPREF` (`web_contentgroup_id`);

--
-- Indizes für die Tabelle `web_pages_groups`
--
ALTER TABLE `web_pages_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `PAGEGROUP` (`group_id`);

--
-- Indizes für die Tabelle `web_pages_headlinks`
--
ALTER TABLE `web_pages_headlinks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `PAGEMETALINKS` (`web_pages_id`),
  ADD KEY `PAGEMETAMEDIA` (`web_medias_id`);

--
-- Indizes für die Tabelle `web_pages_maintenance`
--
ALTER TABLE `web_pages_maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `web_pages_parameter`
--
ALTER TABLE `web_pages_parameter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `HOSTIDENTREF` (`host_id`),
  ADD KEY `PREFERENCESREF` (`web_preferences_id`);

--
-- Indizes für die Tabelle `web_preferences`
--
ALTER TABLE `web_preferences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `HOSTNAME` (`host`),
  ADD KEY `HOSTIDENT` (`host_id`);

--
-- Indizes für die Tabelle `web_redirect`
--
ALTER TABLE `web_redirect`
  ADD PRIMARY KEY (`id`),
  ADD KEY `REDIRECT` (`redirect`),
  ADD KEY `WEBPAGES` (`web_pages_id`);

--
-- Indizes für die Tabelle `web_tags`
--
ALTER TABLE `web_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `WEBTAGID` (`tag_name`);

--
-- Indizes für die Tabelle `web_tags_assign`
--
ALTER TABLE `web_tags_assign`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `WEBTAGASSIGN` (`tag_area`,`web_item_id`,`web_tag_id`),
  ADD KEY `web_tag_id` (`web_tag_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `web_tags_assign`
--
ALTER TABLE `web_tags_assign`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`fieldtypes_id`) REFERENCES `field_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `contact_group_index`
--
ALTER TABLE `contact_group_index`
  ADD CONSTRAINT `indexcontactgroup` FOREIGN KEY (`index_group_id`) REFERENCES `contact_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `mcevent_dates`
--
ALTER TABLE `mcevent_dates`
  ADD CONSTRAINT `mcevent_dates_ibfk_1` FOREIGN KEY (`calendar_id`) REFERENCES `mcevent_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mcevent_dates_ibfk_3` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `mcevent_index`
--
ALTER TABLE `mcevent_index`
  ADD CONSTRAINT `mcevent_index_ibfk_1` FOREIGN KEY (`calendar_id`) REFERENCES `mcevent_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mcevent_index_ibfk_2` FOREIGN KEY (`groups_id`) REFERENCES `mcevent_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `users5`
--
ALTER TABLE `users5`
  ADD CONSTRAINT `users5_ibfk_3` FOREIGN KEY (`contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `users5_ibfk_4` FOREIGN KEY (`user_roles_id`) REFERENCES `user_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `user_acl_index`
--
ALTER TABLE `user_acl_index`
  ADD CONSTRAINT `user_acl_index_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users5` (`id`),
  ADD CONSTRAINT `user_acl_index_ibfk_3` FOREIGN KEY (`acl_group_id`) REFERENCES `user_acl_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `web_content`
--
ALTER TABLE `web_content`
  ADD CONSTRAINT `web_content_ibfk_1` FOREIGN KEY (`web_medias_id`) REFERENCES `web_medias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `web_content_groups`
--
ALTER TABLE `web_content_groups`
  ADD CONSTRAINT `web_content_groups_ibfk_1` FOREIGN KEY (`web_content_id`) REFERENCES `web_content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `web_content_indexgroup`
--
ALTER TABLE `web_content_indexgroup`
  ADD CONSTRAINT `web_content_indexgroup_ibfk_1` FOREIGN KEY (`groups_id`) REFERENCES `web_content_namegroup` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `web_content_indexgroup_ibfk_2` FOREIGN KEY (`indexgroup_id`) REFERENCES `web_content_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `web_forms_field`
--
ALTER TABLE `web_forms_field`
  ADD CONSTRAINT `web_forms_field_ibfk_1` FOREIGN KEY (`web_forms_id`) REFERENCES `web_forms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `web_forms_field_ibfk_2` FOREIGN KEY (`web_medias_id`) REFERENCES `web_medias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `web_forms_fieldoptions`
--
ALTER TABLE `web_forms_fieldoptions`
  ADD CONSTRAINT `web_forms_fieldoptions_ibfk_2` FOREIGN KEY (`form_field_id`) REFERENCES `web_forms_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `web_maps_data`
--
ALTER TABLE `web_maps_data`
  ADD CONSTRAINT `web_maps_data_ibfk_1` FOREIGN KEY (`web_maps_id`) REFERENCES `web_maps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `web_media_categories`
--
ALTER TABLE `web_media_categories`
  ADD CONSTRAINT `web_media_categories_ibfk_1` FOREIGN KEY (`web_medias_id`) REFERENCES `web_medias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `web_media_categories_ibfk_2` FOREIGN KEY (`web_mediagroup_id`) REFERENCES `web_media_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `web_navigation_tree`
--
ALTER TABLE `web_navigation_tree`
  ADD CONSTRAINT `web_navigation_tree_ibfk_1` FOREIGN KEY (`web_pages_id`) REFERENCES `web_pages_parameter` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `web_navigation_tree_ibfk_2` FOREIGN KEY (`web_navigation_id`) REFERENCES `web_navigations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `web_pages_attributes`
--
ALTER TABLE `web_pages_attributes`
  ADD CONSTRAINT `web_pages_attributes_ibfk_1` FOREIGN KEY (`web_pages_id`) REFERENCES `web_pages_parameter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `web_pages_content`
--
ALTER TABLE `web_pages_content`
  ADD CONSTRAINT `web_pages_content_ibfk_3` FOREIGN KEY (`web_pages_id`) REFERENCES `web_pages_parameter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `web_pages_content_ibfk_4` FOREIGN KEY (`web_contentgroup_id`) REFERENCES `web_content_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `web_pages_headlinks`
--
ALTER TABLE `web_pages_headlinks`
  ADD CONSTRAINT `web_pages_headlinks_ibfk_1` FOREIGN KEY (`web_medias_id`) REFERENCES `web_medias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `web_pages_headlinks_ibfk_2` FOREIGN KEY (`web_pages_id`) REFERENCES `web_pages_parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints der Tabelle `web_pages_parameter`
--
ALTER TABLE `web_pages_parameter`
  ADD CONSTRAINT `web_pages_parameter_ibfk_1` FOREIGN KEY (`web_preferences_id`) REFERENCES `web_preferences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `web_redirect`
--
ALTER TABLE `web_redirect`
  ADD CONSTRAINT `web_redirect_ibfk_1` FOREIGN KEY (`web_pages_id`) REFERENCES `web_pages_parameter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

SET FOREIGN_KEY_CHECKS=1;
<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 28.11.2017
 * Time: 15:18
 */

namespace Mcevent\View\Helper\Event;


use Contentinum\View\Helper\AbstractContentHelper;

class ArchivDatesAnually extends AbstractContentHelper
{
    const VIEW_TEMPLATE = 'eventarchiv';

    /**
     *
     * @var string
     */
    protected $headline;

    /**
     *
     * @var string
     */
    protected $linktitle;

    /**
     *
     * @var unknown
     */
    protected $wrapper;

    /**
     *
     * @var unknown
     */
    protected $elements;

    /**
     *
     * @var unknown
     */
    protected $format;

    /**
     *
     * @var unknown
     */
    protected $properties = array(
        'headline',
        'linktitle',
        'wrapper',
        'elements',
        'format'
    );

    public function __invoke($entries, $template, $media)
    {
        $templateKey = static::VIEW_TEMPLATE;
        if (isset($entries['modulFormat'])) {
            $templateKey = $entries['modulFormat'];
        }
        $this->setTemplate($template->plugins->$templateKey);
        $html = '';
        $elements = $this->elements->toArray();
        foreach ($entries['modulContent'] as $row) {
            $elements["grid"]["attr"]['href'] = '/' . $this->view->paramter['pages'] . '/' . $row['year'];
            $elements["grid"]["attr"]['title'] = $this->linktitle . ' ' . $row['year'];
            $html .= $this->deployRow($elements, 'Jahr ' . $row['year']);
        }
        if (strlen($html) > 1) {
            $wrapper = $this->wrapper->toArray();
            if (isset($wrapper['row']) && null != $this->headline) {
                $wrapper['row']['content:before'] = '<h3>' . $this->headline . '</h3>';
            }
            $html = $this->deployRow($wrapper, $html);
        }
        return $html;
    }
}
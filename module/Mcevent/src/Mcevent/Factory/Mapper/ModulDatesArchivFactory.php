<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 28.11.2017
 * Time: 15:34
 */

namespace Mcevent\Factory\Mapper;


use Mcevent\Mapper\ModulDatesArchiv;
use Zend\ServiceManager\ServiceLocatorInterface;

use Contentinum\Factory\EntityManagerFactory;

class ModulDatesArchivFactory extends EntityManagerFactory
{
    /**
     * (non-PHPdoc)
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ModulDatesArchiv($serviceLocator->get(self::MC_ENTITYMANAGER));
    }
}
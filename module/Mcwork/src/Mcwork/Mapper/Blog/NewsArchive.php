<?php
/*
 *
 */

namespace Mcwork\Mapper\Blog;


use ContentinumComponents\Mapper\Worker;

/**
 * Class NewsArchive
 * @package Mcwork\Mapper\Blog
 */
class NewsArchive extends Worker
{
    /**
     * @param array|null $params
     * @return array
     */
    public function fetchContent(array $params = null)
    {
        return [];
    }

    /**
     * @param array|null $params
     * @param null $posts
     * @return array
     */
    public function processRequest(array $params = null, $posts = null)
    {
        if (is_array($posts)){
            $params = array_merge($params,$posts);
        }
        return $this->fetchContent($params);
    }
}

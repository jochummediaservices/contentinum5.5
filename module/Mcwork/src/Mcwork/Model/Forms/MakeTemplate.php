<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 26.04.2018
 * Time: 09:57
 */

namespace Mcwork\Model\Forms;


use ContentinumComponents\Filter\Url\Prepare;
use Mcwork\Model\Delete\AbstractEntries;

class MakeTemplate extends AbstractEntries
{
    private $pathToTemplate = '/data/files/emailtemplates';

    /**
     *
     * @param unknown $id
     * @return string
     */
    public function execute($id)
    {
        $sql = "SELECT wff.label, wff.field_typ, wff.field_name, wf.headline FROM web_forms_field AS wff ";
        $sql .= "LEFT JOIN web_forms AS wf ON wf.id = wff.web_forms_id ";
        $sql .= "WHERE wff.web_forms_id = {$id};";
        $formRows = $this->fetchAll($sql);
        $emailBody = false;
        $filename = 'formtemplate' . $id;
        if (is_array($formRows) && !empty($formRows)){
            $emailBody = "\nServerzeit: {SERVERTIME}";
            $emailBody .= "\n\n";
            foreach ($formRows as $row){
                if (isset($row['headline'])){
                    $filename = $row['headline'];
                }
                if ($row['field_typ'] != 'Button'){
                    $emailBody .= "\n" . $row['label'] . ": {" . strtoupper($row['field_name']) . "}";
                }
            }
            $emailBody .= "\n\n\n{DISCLAIMER}";
            $emailBody .= "\n\n[ Dies ist eine automatisch durch das Webformular generierte E-Mail. ]";

            $filter = new Prepare();
            $filename = 'formular-' . $filter->filter($filename);

            file_put_contents(CON_ROOT_PATH . $this->pathToTemplate . '/' . $filename, $emailBody);
            $this->executeQuery("UPDATE web_forms SET email_template ='{$filename}' WHERE id = {$id}");
            $this->setMessage('create_form_mail_template');
            return 'success';
        } else {
            $this->setMessage('form_has_no_fields');
            return 'warn';
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 26.04.2018
 * Time: 09:42
 */

namespace Mcwork\Controller;


use Contentinum\Controller\AbstractApplicationController;
use Zend\View\Model\ViewModel;

/**
 * Class MakeController
 * @package Mcwork\Controller
 */
class MakeController extends AbstractApplicationController
{
    /**
     * Query parameter,default 'category'
     *
     * @var string
     */
    protected $querykey = 'category';


    public function application($pageOptions, $role = null, $acl = null)
    {

        if (false !== $pageOptions->getApp('querykey')) {
            $this->querykey = $pageOptions->getApp('querykey');
        }

        $redirectUrl = $pageOptions->getApp('settoroute');
        $id = $this->params()->fromRoute($this->querykey, 0);

        $cat = '';
        if (null !== ($cat = $this->params()->fromRoute('categoryvalue', null))) {
            $cat = '/category/' . $cat;
        }

        if (false === $id) {
            if (true === $this->getXmlHttpRequest()) {
                return $this->buildView(array('error' => 'miss_parmeter'));
            } else {
                return $this->redirect()->toUrl($redirectUrl . $cat);
            }
        }

        try {

            if (false !== ($relatedEntries = $pageOptions->getApp('hasEntries'))){
                $this->worker->setRelatedEntries($relatedEntries);
            }

            switch ($this->worker->execute($id)) {
                case 'success':
                    if (true === $this->getXmlHttpRequest()) {
                        return $this->buildView(array('success' => $this->worker->getMessage()));
                    } else {
                        $this->flashMessenger()
                            ->setNamespace('mcwork')
                            ->addSuccessMessage($this->worker->getMessage());
                    }
                    break;
                case 'warn':
                default:
                    if (true === $this->getXmlHttpRequest()) {
                        return $this->buildView(array('warn' => $this->worker->getMessage()));
                    } else {
                        $this->flashMessenger()
                            ->setNamespace('mcwork')
                            ->addWarningMessage($this->worker->getMessage());
                    }
            }
        } catch (\Exception $e) {
            if (true === $this->getXmlHttpRequest()) {
                return $this->buildView(array('error' => $e->getMessage()));
            } else {
                $this->flashMessenger()
                    ->setNamespace('mcwork')
                    ->addErrorMessage($e->getMessage());
            }
        }

        return $this->redirect()->toUrl($redirectUrl . $cat);

    }

    /*
     * (non-PHPdoc)
     * @see \Contentinum\Controller\AbstractApplicationController::process()
     */
    protected function process($pageOptions, $role = null, $acl = null)
    {
        return $this->application($pageOptions, $role, $acl);
    }

    /**
     *
     * @param unknown $datas
     */
    protected function buildView($datas)
    {
        $view = $view = new ViewModel(array(
            'entries' => $datas
        ));
        $view->setTemplate('content/forms/maketemplate');
        return $view;
    }

}
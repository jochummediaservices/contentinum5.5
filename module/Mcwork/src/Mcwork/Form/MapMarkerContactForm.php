<?php
/**
 * contentinum - accessibility websites
 *
 * LICENSE
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @category contentinum backend
 * @package Forms
 * @author Michael Jochum, michael.jochum@jochum-mediaservices.de
 * @copyright Copyright (c) 2009-2013 jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 * @license http://www.opensource.org/licenses/bsd-license
 * @since contentinum version 5.0
 * @link      https://github.com/Mikel1961/contentinum-components
 * @version   1.0.0
 */
namespace Mcwork\Form;

use ContentinumComponents\Forms\AbstractForms;

/**
 * contentinum mcwork form fieldtypes
 * 
 * @author Michael Jochum, michael.jochum@jochum-mediaservices.de
 */
class MapMarkerContactForm extends AbstractForms
{

    /**
     * form field elements
     * 
     * @see \ContentinumComponents\Forms\AbstractForms::elements()
     */
    public function elements()
    {
        return array(
            array(
                'spec' => array(
                    'name' => 'name',
                    'required' => true,

                    'options' => array(
                        'label' => 'Name',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW)
                    ),

                    'type' => 'Text',
                    'attributes' => array(
                        'required' => 'required',
                        'id' => 'name'
                    )
                ),
            ),
            array(
                'spec' => array(
                    'name' => 'phone',
                    
                    'options' => array(
                        'label' => 'Telefon',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                    ),
                    
                    'type' => 'Text',
                    'attributes' => array(
                        'id' => 'phone'
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'email',
                    'options' => array(
                        'label' => 'E-Mail',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                    ),
                    
                    'type' => 'Text',
                    'attributes' => array(
                        'id' => 'email'
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'url',
                    'options' => array(
                        'label' => 'Internet',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                    ),

                    'type' => 'Text',
                    'attributes' => array(
                        'id' => 'url'
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'businessHours',
                    'required' => false,

                    'options' => array(
                        'label' => 'Öffnungszeiten',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                        'description' => 'pm Öffnungszeiten am Vormittag, am Öffnungszeiten am Nachmittag, d für Ganztägige Öffnungszeiten, c An diesem Tag geschlossen. Abschließen mit Semikolon außer am So. Alle Tage müssen genannt werden.',


                    ),
                    'type' => 'Textarea',
                    'attributes' => array(
                        'rows' => '8',
                        'id' => 'businessHours',
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'businessHoursUse',
                    'required' => true,
                    'options' => array(
                        'label' => 'Öffnungszeitentabelle verwenden?',
                        'value_options' => array(
                            'no' => 'Öffnungszeiten nicht anzeigen (Standard)',
                            'yes' => 'Öffnungszeiten anzeigen (Tabelle Öffnungszeiten nach dem Muster ausfüllen',
                        ),
                        'deco-row' => $this->getDecorators(self::DECO_TAB_ROW)
                    ),
                    'type' => 'Select',
                    'attributes' => array(
                        'required' => 'required',
                        'id' => 'businessHoursUse',
                        'value' => 'no'
                    )
                )
            ),
        );
    }

    protected function openingTemplate()
    {
        $str = "Mo=am?00:00-00:00.pm?00:00-00:00;";
        $str .= "\nDi=am?00:00-00:00.pm?00:00-00:00;";
        $str .= "\nMi=am?00:00-00:00.pm?00:00-00:00;";
        $str .= "\nDo=am?00:00-00:00.pm?00:00-00:00;";
        $str .= "\nFr=am?00:00-00:00.pm?00:00-00:00;";
        $str .= "\nSa=d?00:00-00:00;";
        $str .= "\nSo=c";
        return $str;
    }

    /**
     * form input filter and validation
     * 
     * @see \ContentinumComponents\Forms\AbstractForms::filter()
     */
    public function filter()
    {
        return array(
            'name' => array(
                'required' => true,
            ),
            'phone' => array(
                'required' => false,
            ),
            'email' => array(
                'required' => false,
            ),
            'url' => array(
                'required' => false,
            ),
            'business_hours' => array(
                'required' => false,
            ),

        );
    }

    /**
     * initiation and get form
     * 
     * @see \ContentinumComponents\Forms\AbstractForms::getForm()
     */
    public function getForm()
    {
        return $this->factory->createForm(array(
            'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
            'elements' => $this->elements(),
            'input_filter' => $this->filter()
        ));
    }
}
<?php
/**
 * contentinum - accessibility websites
 *
 * LICENSE
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @category contentinum backend
 * @package Forms
 * @author Michael Jochum, michael.jochum@jochum-mediaservices.de
 * @copyright Copyright (c) 2009-2013 jochum-mediaservices, Katja Jochum (http://www.jochum-mediaservices.de)
 * @license http://www.opensource.org/licenses/bsd-license
 * @since contentinum version 5.0
 * @link      https://github.com/Mikel1961/contentinum-components
 * @version   1.0.0
 */
namespace Mcwork\Form\Messages;

use ContentinumComponents\Forms\AbstractForms;

/**
 * contentinum mcwork form fieldtypes
 * 
 * @author Michael Jochum, michael.jochum@jochum-mediaservices.de
 */
class MessageForm extends AbstractForms
{

    /**
     * form field elements
     * 
     * @see \ContentinumComponents\Forms\AbstractForms::elements()
     */
    public function elements()
    {
        return array(
            array(
                'spec' => array(
                    'name' => 'formRowStart',
                    'options' => array(
                        'fieldset' => array(
                            'nofieldset' => 1
                        )
                    ),
                    'type' => 'ContentinumComponents\Forms\Elements\Note',
                    'attributes' => array(
                        'id' => 'formColumStart',
                        'value' => '<div class="row">'
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'headline',
                    'required' => true,
                    
                    'options' => array(
                        'label' => 'Headline',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                    ),
                    
                    'type' => 'Text',
                    'attributes' => array(
                        'required' => 'required',
                        'id' => 'headline',
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'content',
                    'required' => false,
                    'options' => array(
                        'label' => 'Meldung',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                    ),
                    'type' => 'Textarea',
                    'attributes' => array(
                        'rows' => '8',
                        'id' => 'content',
                        'class' => 'contributioncontent',
                    )
                )
            ),

            array(
                'spec' => array(
                    'name' => 'webMediasId',
                    'required' => true,
                    'options' => array(
                        'label' => 'Bild zur Meldung auswählen',
                        'empty_option' => 'Please select',
                        'value_options' => $this->getServiceLocator()->get('mcwork_public_media'),
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                        'fieldset' => array(
                            'legend' => 'Messages',
                            'attributes' => array(
                                'class' => 'medium-7 columns',
                                'id' => 'fieldsetContribution'
                            )
                        )
                    ),

                    'type' => 'Select',
                    'attributes' => array(
                        'required' => 'required',
                        'id' => 'webMediasId',
                        'value' => 1,
                        'class' => 'chosen-select'
                    )
                )
            ),

            array(
                'spec' => array(
                    'name' => 'messagesgroup',
                    'required' => true,
                    'options' => array(
                        'label' => 'Gruppe',
                        'empty_option' => 'Please select',
                        'value_options' => $this->getSelectOptions('messagesgroup'),


                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                    ),
                    'type' => 'Select',
                    'attributes' => array(
                        'required' => 'required',
                        'id' => 'messagesgroup'
                    )
                )
            ),


            array(
                'spec' => array(
                    'name' => 'publishAuthor',
                    'required' => false,
                    'options' => array(
                        'label' => 'Publish Author',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW)
                    ),
                    'type' => 'Text',
                    'attributes' => array(
                        'id' => 'publishAuthor'
                    )
                )
            ),

            array(
                'spec' => array(
                    'name' => 'authorEmail',
                    'required' => false,
                    'options' => array(
                        'label' => 'Author Email',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW)
                    ),
                    'type' => 'Text',
                    'attributes' => array(
                        'id' => 'authorEmail'
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'publishDate',
                    'required' => false,
                    'options' => array(
                        'label' => 'Datum',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW)
                    ),
                    'type' => 'Text',
                    'attributes' => array(
                        'id' => 'publishDate',
                        'value' => date('Y-m-d H:i')
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'publish',
                    'required' => false,
                    'options' => array(
                        'label' => 'Publish',
                        'empty_option' => 'Please select',
                        'value_options' => $this->getOptions('attribute_publish'),
                        'deco-row' => $this->getDecorators(self::DECO_TAB_ROW)
                    ),
                    'type' => 'Select',
                    'attributes' => array(
                        'id' => 'publish'
                    )
                )
            ),

            array(
                'spec' => array(
                    'name' => 'publishUp',
                    'required' => false,
                    'options' => array(
                        'label' => 'Publication start',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW)
                    ),
                    'type' => 'Text',
                    'attributes' => array(
                        'id' => 'publishUp',
                    )
                )
            ),

            array(
                'spec' => array(
                    'name' => 'publishDown',
                    'required' => false,
                    'options' => array(
                        'label' => 'Publication end',
                        'deco-row' => $this->getDecorators(self::DECO_ELM_ROW),
                        'fieldset' => array(
                            'legend' => 'Messages',
                            'attributes' => array(
                                'class' => 'medium-5 columns',
                                'id' => 'fieldsetContribution'
                            )
                        )
                    ),
                    'type' => 'Text',
                    'attributes' => array(
                        'id' => 'publishDown',
                    )
                )
            ),
            array(
                'spec' => array(
                    'name' => 'formRowEnd',
                    'options' => array(
                        'fieldset' => array(
                            'nofieldset' => 1
                        )
                    ),
                    'type' => 'ContentinumComponents\Forms\Elements\Note',
                    'attributes' => array(
                        'id' => 'formColumnEnd',
                        'value' => '</div>'
                    )
                )
            )
        );
    }

    /**
     * form input filter and validation
     * 
     * @see \ContentinumComponents\Forms\AbstractForms::filter()
     */
    public function filter()
    {
        return array(
            'headline' => array(
                'required' => true,
            ),

            'content'  => array(
                'required' => false,
            ),

            'webMediasId' => array(
                'required' => true,
            ),
            'messagesgroup' => array(
                'required' => true,
            ),
            'publishAuthor' => array(
                'required' => false,
            ),

            'authorEmail' => array(
                'required' => false,
            ),

            'publish' => array(
                'required' => false,
            ),


            'publishUp' => array(
                'required' => false,
            ),

            'publishDown' => array(
                'required' => false
            )
        );
    }

    /**
     * initiation and get form
     * 
     * @see \ContentinumComponents\Forms\AbstractForms::getForm()
     */
    public function getForm()
    {
        return $this->factory->createForm(array(
            'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
            'elements' => $this->elements(),
            'input_filter' => $this->filter()
        ));
    }
}
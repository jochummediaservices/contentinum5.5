��    J      l  e   �      P     Q      f     �     �     �     �     �     �     �                       
        #     (     0     5     <     D     I  	   O     Y     ^     e     n     t     �     �     �     �     �  
   �     �     �     �     �     �     �  
             %     -     @  
   G     R     W  
   i     t     y     �     �  +   �  G   �     	     2	  2   F	  M   y	  .   �	  <   �	     3
  	   9
     C
     P
  9   V
     �
  "   �
     �
     �
     �
     �
     �
     �
  �  �
     �     �               3     L     a     m     u  	   ~     �     �  
   �  
   �     �     �     �     �     �  
   �     �     �     �     �                     '     /     ;     D     L     X      e     �     �     �     �     �     �     �     �     �  
          	              6     F     N  
   a     l  L   t  L   �  M     2   \  H   �  N   �  0   '  M   X     �     �     �  	   �  a   �     =  )   B     l     s     w     |     �  H   �     $   A      H          )               C          &   E   7   J   +       	   G      I       -   <   %       
       1         #       .   ?   6         8       0      F       :   4   2   5             >       3       /         D              "           ;      @             !                 B   9   '   =          ,             *   (                                      A 404 error occurred A record matching this was found Additional information Administrator Resource All rights reserved. An error occurred Author Resource Back Business title Cancel City Clear Close Controller Copy Country Date Delete Display Edit Email Exception File Folder Headline Hello Help &amp; Support Language Manager Resource Member Message Move New Folder No Exception available Not Publicly Number Phone Please select Previous exceptions Properties Public Publish Publisher Resource Rename Salutation Save Search and select Select All Size Stack trace State or province Street Supplied credential is invalid, second try! Supplied credential is invalid, third try! The account has been locked! Supplied credential is invalid. Supplied_credential The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. Title Unpublish Unselect All Unzip We cannot determine at this time why a 404 was generated. Year Your user entries are not correct! Zip Zipcode mr ms resolves to %s user_locked Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-07-05 23:45-0700
PO-Revision-Date: 2017-11-07 08:54+0100
Last-Translator: Michael Jochum <michael.jochum@jochum-mediaservices.de>
Language-Team: ZF Contributors <zf-devteam@zend.com>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 2.0.4
X-Poedit-SearchPath-0: ..
 Es trat ein 404 Fehler auf Dieser Eintrag existiert schon Zusätzliche Information CMS-Administratoren Alle Rechte vorbehalten. Ein Fehler trat auf! CMS-Autoren Zurück Position Abbrechen Stadt Zurücksetzen Schließen Controller Kopieren Land Datum Löschen Anzeigen Bearbeiten E-Mail Ausnahme Datei Ordner Überschrift Hallo Hilfe &amp; Support Sprache CMS-Manager Mitglied Meldung Verschieben Neuer Ordner Es ist keine Ausnahme verfügbar Nicht Öffentlich Nummer Telefon Bitte Auswählen Vorherige Ausnahme Eigenschaften Öffentlich Veröffentlichen CMS-Publisher Umbenennen Anrede Speichern Suchen und Auswählen Alle Auswählen Größe Stapelüberwachung Bundesland Straße Mitgelieferte Berechtigungsnachweis ist ungültig. Sie haben noch 1 Versuch! Mitgelieferte Berechtigungsnachweis ist ungültig! Das Konto wurde gesperrt! Mitgelieferte Berechtigungsnachweis ist ungültig. Sie haben noch 2 Versuche. Mitgelieferte Berechtigungsnachweis ist ungültig. Für die angeforderte URL konnte keine Übereinstimmung gefunden werden. Der angeforderte Controller konnte keiner Controller Klasse zugeordnet werden. Der angeforderte Controller ist nicht aufrufbar. Der angeforderte Controller war nicht in der Lage die Anfrage zu verarbeiten. Titel Nicht Veröffentlicht Alle Abwählen Entpacken Zu diesem Zeitpunkt ist es uns nicht möglich zu bestimmen, warum ein 404 Fehler aufgetreten ist. Jahr Ihre Benutzereingaben sind nicht korrekt! Packen PLZ Herr Frau wird aufgelöst in %s Der Benutzer ist gesperrt, wenden Sie sich bitte an Ihren Administrator. 
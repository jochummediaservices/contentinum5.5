<?php

namespace Contentinum\Entity;

use Doctrine\ORM\Mapping as ORM;
use ContentinumComponents\Entity\AbstractEntity;

/**
 * Messages
 *
 * @ORM\Table(name="web_mgs", indexes={@ORM\Index(name="HEADLINEMSG", columns={"headline"})})
 * @ORM\Entity
 */
class Messages extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="content_category", type="integer", nullable=false)
     */
    private $contentCategory = 0;    

    /**
     * @var string
     *
     * @ORM\Column(name="resource", type="string", length=50, nullable=false)
     */
    private $resource = '';    
    
    /**
     * @var string
     *
     * @ORM\Column(name="resource_group",  type="integer", nullable=false)
     */
    private $resourceGroup = 0;    

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="text", nullable=false)
     */
    private $source = '';

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=6, nullable=false)
     */
    private $lang = 'de';

    /**
     * @var string
     *
     * @ORM\Column(name="headline", type="text", nullable=false)
     */
    private $headline = '';

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content = '';
    
    /**
     * @var string
     *
     * @ORM\Column(name="publish_date", type="string", length=30, nullable=false)
     */
    private $publishDate = '';    
        
    /**
     * @var string
     *
     * @ORM\Column(name="publish_author", type="string", length=100, nullable=false)
     */
    private $publishAuthor = '';
    
    /**
     * @var string
     *
     * @ORM\Column(name="author_email", type="string", length=80, nullable=false)
     */
    private $authorEmail = '';    
    
    /**
     * @var string
     *
     * @ORM\Column(name="publish", type="string", length=10, nullable=false)
     */
    private $publish = 'no';
    
    /**
     * @var string
     *
     * @ORM\Column(name="publish_up", type="string", nullable=false)
     */
    private $publishUp = '0000-00-00 00:00:00';
    
    /**
     * @var string
     *
     * @ORM\Column(name="publish_down", type="string", nullable=false)
     */
    private $publishDown = '0000-00-00 00:00:00';
    
    /**
     * @var string
     *
     * @ORM\Column(name="comment_status", type="boolean", nullable=false)
     */
    private $commentStatus = '0';    

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="update_by", type="integer", nullable=false)
     */
    private $updateBy = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="register_date", type="datetime", nullable=false)
     */
    private $registerDate = '0000-00-00 00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="up_date", type="datetime", nullable=false)
     */
    private $upDate = '0000-00-00 00:00:00';
    
    /**
     *
     * @var \Contentinum\Entity\WebMedia
     *
     * @ORM\ManyToOne(targetEntity="Contentinum\Entity\WebMedias",cascade={"persist"})
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="web_medias_id", referencedColumnName="id")
     * })
     */
    private $webMediasId;

    /**
     *
     * @var \Contentinum\Entity\MessagesGroup
     *
     * @ORM\ManyToOne(targetEntity="Contentinum\Entity\MessagesGroup",cascade={"persist"})
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="web_msg_group_id", referencedColumnName="id")
     * })
     */
    private $messagesgroup = 0;

    /**
     * Construct
     * @param array $options
     */
    public function __construct (array $options = null)
    {
    	if (is_array($options)) {
    		$this->setOptions($options);
    	}
    }
    
    /** (non-PHPdoc)
     * @see \ContentinumComponents\Entity\AbstractEntity::getEntityName()
     */
    public function getEntityName()
    {
    	return get_class($this);
    }
    
    /** (non-PHPdoc)
     * @see \ContentinumComponents\Entity\AbstractEntity::getPrimaryKey()
     */
    public function getPrimaryKey()
    {
    	return 'id';
    }
    
    /** (non-PHPdoc)
     * @see \ContentinumComponents\Entity\AbstractEntity::getPrimaryValue()
     */
    public function getPrimaryValue()
    {
    	return $this->id;
    }
    
    /** (non-PHPdoc)
     * @see \ContentinumComponents\Entity\AbstractEntity::getProperties()
     */
    public function getProperties()
    {
    	return get_object_vars($this);
    }
     
    /**
     * @return the $id
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * @param number $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getContentCategory()
    {
        return $this->contentCategory;
    }

    /**
     * @param int $contentCategory
     */
    public function setContentCategory($contentCategory)
    {
        $this->contentCategory = $contentCategory;
    }

    /**
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    /**
     * @return string
     */
    public function getResourceGroup()
    {
        return $this->resourceGroup;
    }

    /**
     * @param string $resourceGroup
     */
    public function setResourceGroup($resourceGroup)
    {
        $this->resourceGroup = $resourceGroup;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return string
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param string $headline
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param string $publishDate
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;
    }

    /**
     * @return string
     */
    public function getPublishAuthor()
    {
        return $this->publishAuthor;
    }

    /**
     * @param string $publishAuthor
     */
    public function setPublishAuthor($publishAuthor)
    {
        $this->publishAuthor = $publishAuthor;
    }

    /**
     * @return string
     */
    public function getAuthorEmail()
    {
        return $this->authorEmail;
    }

    /**
     * @param string $authorEmail
     */
    public function setAuthorEmail($authorEmail)
    {
        $this->authorEmail = $authorEmail;
    }

    /**
     * @return string
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * @param string $publish
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;
    }

    /**
     * @return string
     */
    public function getPublishUp()
    {
        return $this->publishUp;
    }

    /**
     * @param string $publishUp
     */
    public function setPublishUp($publishUp)
    {
        $this->publishUp = $publishUp;
    }

    /**
     * @return string
     */
    public function getPublishDown()
    {
        return $this->publishDown;
    }

    /**
     * @param string $publishDown
     */
    public function setPublishDown($publishDown)
    {
        $this->publishDown = $publishDown;
    }

    /**
     * @return string
     */
    public function getCommentStatus()
    {
        return $this->commentStatus;
    }

    /**
     * @param string $commentStatus
     */
    public function setCommentStatus($commentStatus)
    {
        $this->commentStatus = $commentStatus;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return int
     */
    public function getUpdateBy()
    {
        return $this->updateBy;
    }

    /**
     * @param int $updateBy
     */
    public function setUpdateBy($updateBy)
    {
        $this->updateBy = $updateBy;
    }

    /**
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @param \DateTime $registerDate
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpDate()
    {
        return $this->upDate;
    }

    /**
     * @param \DateTime $upDate
     */
    public function setUpDate($upDate)
    {
        $this->upDate = $upDate;
    }

    /**
     * @return WebMedia
     */
    public function getWebMediasId()
    {
        return $this->webMediasId;
    }

    /**
     * @param WebMedia $webMediasId
     */
    public function setWebMediasId($webMediasId)
    {
        $this->webMediasId = $webMediasId;
    }

    /**
     * @return MessagesGroup
     */
    public function getMessagesgroup()
    {
        return $this->messagesgroup;
    }

    /**
     * @param MessagesGroup $messagesgroup
     */
    public function setMessagesgroup($messagesgroup)
    {
        $this->messagesgroup = $messagesgroup;
    }

}
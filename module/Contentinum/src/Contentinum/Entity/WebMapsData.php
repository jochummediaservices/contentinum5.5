<?php

namespace Contentinum\Entity;

use Doctrine\ORM\Mapping as ORM;
use ContentinumComponents\Entity\AbstractEntity;

/**
 * WebMapsData
 *
 * @ORM\Table(name="web_maps_data", indexes={@ORM\Index(name="MAPSPARENT", columns={"web_maps_id"}), @ORM\Index(name="MAPSMEDIA", columns={"web_medias_id"})})
 * @ORM\Entity
 */
class WebMapsData extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;  

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=50, nullable=false)
     */
    private $latitude = '';

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=50, nullable=false)
     */
    private $longitude = '';
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="startzoom", type="integer", nullable=false)
     */
    private $startzoom = 12;    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="map_marker", type="string", length=250, nullable=false)
     */    
    private $mapMarker = '_nomedia';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=250, nullable=false)
     */
    private $street = '';

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=250, nullable=false)
     */
    private $city = '';


    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=false)
     */
    private $phone = '';


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150, nullable=false)
     */
    private $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=250, nullable=false)
     */
    private $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description = '';

    /**
     * @var string
     *
     * @ORM\Column(name="business_hours", type="text", length=65535, nullable=false)
     */
    private $businessHours = '';

    /**
     * @var string
     *
     * @ORM\Column(name="business_hours_use", type="string", length=4, nullable=false)
     */
    private $businessHoursUse = 'no';

    /**
     * @var string
     *
     * @ORM\Column(name="publish", type="string", length=10, nullable=false)
     */
    private $publish = 'no';

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="update_by", type="integer", nullable=false)
     */
    private $updateBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="register_date", type="datetime", nullable=false)
     */
    private $registerDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="up_date", type="datetime", nullable=false)
     */
    private $upDate;

    /**
     * @var \Contentinum\Entity\WebMaps
     *
     * @ORM\ManyToOne(targetEntity="Contentinum\Entity\WebMaps")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="web_maps_id", referencedColumnName="id")
     * })
     */
    private $webMaps;
    
    /**
     *
     * @var \Contentinum\Entity\WebMedia
     *
     * @ORM\ManyToOne(targetEntity="Contentinum\Entity\WebMedias",cascade={"persist"})
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="web_medias_id", referencedColumnName="id")
     * })
     */
    private $webMediasId;    
    
    /**
     * Construct
     * @param array $options
     */
    public function __construct (array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    /** (non-PHPdoc)
     * @see \ContentinumComponents\Entity\AbstractEntity::getEntityName()
     */
    public function getEntityName()
    {
        return get_class($this);
    }
    
    /** (non-PHPdoc)
     * @see \ContentinumComponents\Entity\AbstractEntity::getPrimaryKey()
     */
    public function getPrimaryKey()
    {
        return 'id';
    }
    
    /** (non-PHPdoc)
     * @see \ContentinumComponents\Entity\AbstractEntity::getPrimaryValue()
     */
    public function getPrimaryValue()
    {
        return $this->id;
    }
    
    /** (non-PHPdoc)
     * @see \ContentinumComponents\Entity\AbstractEntity::getProperties()
     */
    public function getProperties()
    {
        return get_object_vars($this);
    }
    
    /**
     * @param number $id
     *
     * @return Accounts
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * @return the $latitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

	/**
     * @param string $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

	/**
     * @return the $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

	/**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }
    
    /**
     * @return the $startzoom
     */
    public function getStartzoom()
    {
        return $this->startzoom;
    }
    
    /**
     * @param boolean $startzoom
     */
    public function setStartzoom($startzoom)
    {
        $this->startzoom = $startzoom;
    }    

	/**
     * @return the $mapMarker
     */
    public function getMapMarker()
    {
        return $this->mapMarker;
    }

	/**
     * @param number $mapMarker
     */
    public function setMapMarker($mapMarker)
    {
        $this->mapMarker = $mapMarker;
    }

	/**
     * @return the $name
     */
    public function getName()
    {
        return $this->name;
    }

	/**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

	/**
     * @return the $street
     */
    public function getStreet()
    {
        return $this->street;
    }

	/**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

	/**
     * @return the $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

	/**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }


    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

	/**
     * @return the $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getBusinessHours()
    {
        return $this->businessHours;
    }

    /**
     * @param string $businessHours
     */
    public function setBusinessHours($businessHours)
    {
        $this->businessHours = $businessHours;
    }

    /**
     * @return string
     */
    public function getBusinessHoursUse()
    {
        return $this->businessHoursUse;
    }

    /**
     * @param string $businessHoursUse
     */
    public function setBusinessHoursUse($businessHoursUse)
    {
        $this->businessHoursUse = $businessHoursUse;
    }

	/**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

	/**
     * @return the $publish
     */
    public function getPublish()
    {
        return $this->publish;
    }

	/**
     * @param string $publish
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;
    }

	/**
     * @return the $createdBy
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

	/**
     * @param number $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

	/**
     * @return the $updateBy
     */
    public function getUpdateBy()
    {
        return $this->updateBy;
    }

	/**
     * @param number $updateBy
     */
    public function setUpdateBy($updateBy)
    {
        $this->updateBy = $updateBy;
    }

	/**
     * @return the $registerDate
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

	/**
     * @param DateTime $registerDate
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

	/**
     * @return the $upDate
     */
    public function getUpDate()
    {
        return $this->upDate;
    }

	/**
     * @param DateTime $upDate
     */
    public function setUpDate($upDate)
    {
        $this->upDate = $upDate;
    }

	/**
     * @return the $webMaps
     */
    public function getWebMaps()
    {
        return $this->webMaps;
    }

	/**
     * @param \Contentinum\Entity\WebMaps $webMaps
     */
    public function setWebMaps($webMaps)
    {
        $this->webMaps = $webMaps;
    }
    /**
     * @return the $webMediasId
     */
    public function getWebMediasId()
    {
        return $this->webMediasId;
    }

    /**
     * @param \Contentinum\Entity\WebMedia $webMediasId
     */
    public function setWebMediasId($webMediasId)
    {
        $this->webMediasId = $webMediasId;
    }


}
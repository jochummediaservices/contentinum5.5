<?php
/**
 * Created by PhpStorm.
 * User: michael.jochum
 * Date: 12.12.2017
 * Time: 14:08
 */

namespace Contentinum\View\Helper;


class Messages extends AbstractContentHelper
{
    /**
     * @var array
     */
    protected $wrapper;

    /**
     * @var array
     */
    protected $messages;

    /**
     * @var array
     */
    protected $header;

    /**
     * @var array
     */
    protected $headline;

    /**
     * @var array
     */
    protected $publishDate;

    /**
     * @var array
     */
    protected $publishAuthor;

    /**
     * @var array
     */
    protected $footer;

    /**
     * @var array
     */
    protected $media;


    /**
     * @param $entries
     * @param $template
     * @param $media
     * @return string
     */
    public function __invoke($entries, $template, $media)
    {
        $html = '';
        foreach ($entries['modulContent'] as $entry) {
            $msg = '';

            $msg .= $this->deployRow($this->header, $this->deployRow($this->headline,$entry->headline));
            $msg .= $entry->content;

            $html .= $this->deployRow($this->messages, $msg);

        }
        if ($this->wrapper){
            $html = $this->deployRow($this->wrapper, $html);
        }
        return $html;
    }
}
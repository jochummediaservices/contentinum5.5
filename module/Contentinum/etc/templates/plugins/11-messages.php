<?php
return array(
    'plugins' => array(
        
        'messagesblog' => array(
            'viewhelper' => 'messages',
            'key' => 'messagesblog',
            'name' => 'Standard Meldungen',

        
            'wrapper' => array(
                'grid' => array(
                    'element' => 'div',
                    'attr' => array(
                        'class' => 'messages'
                    )
                )
            ),
            'messages' => array(
                'grid' => array(
                    'element' => 'article',
                    'attr' => array(
                        'class' => 'messages-article'
                    )
                )
            ),
            'header' => array(
                'grid' => array(
                    'element' => 'header',
                    'attr' => array(
                        'class' => 'messages-article-header'
                    )
                )
            ),

            'headline' => array(
                'grid' => array(
                    'element' => 'h2',
                    'attr' => array(),
                     
                ),
            ),
        
            'publishDate' => array(
                'grid' => array(
                    'format' => array('dateFormat' => array( 'attr' => 'FULL')),
                    'element' => 'time',
                    'attr' => array(
                        'class' => 'messages-date'
                    ),
                ),
            ),
        
            'media' => array(
                'row' => array(
                    'element' => 'figure',
                    'attr' => array(
                        'class' => 'messages-media'
                    )
                ),
                'grid' => array(
                    'element' => 'figcaption',
                    'attr' => array(
                        'class' => 'messages-article-caption'
                    )
                )
            ),
        

        

        
            'footer' => array(
                'grid' => array(
                    'element' => 'footer',
                    'attr' => array(
                        'class' => 'messages-article-footer'
                    )
                )
            ),
            'publishAuthor' => array(
                'grid' => array(
                    'element' => 'span',
                    'attr' => array(
                        'class' => 'messages-article-author'
                    ),
                    'content:before' => ', '
        
                )
            ),


        
        ),   
        
        

        
        )
);